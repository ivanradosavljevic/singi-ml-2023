# Vežbe 1
*Cilj vežbi: Upoznavanje sa osnovnim obradama podataka, osnovni koncepti mašinskog učenja.*

1. Napraviti virtualno okruženje i pokrenuti naredbe za instalaciju:
    ```
    python -m pip install jupyterlab
    python -m pip install pandas
    python -m pip install matplotlib
    python -m pip install seaborn
    python -m pip install scikit-learn
    ```
2. Učitati podatke iz datoteke "IPB Corona - avg - half hour - final.xlsx".
3. Prikazati učitane podatke, prilikom prikazivanja prikazati sve kolone iz tabele.
4. Zapisati učitane podatke kao csv datoteku.
5. Idvojiti kolone date i NTtemp i prikazati njihov sadržaj.
6. Iscrtati grafikon za kolone date i NTtemp. Koristiti linechart.
7. Ispisati osnovne statističke podatke za učitanu datoteku.
8. Izbaciti kolonu "NTwxcodes" i izbaciti sve redove gde se nalaze prazne kolone. Ponovo prikazati statističke podatke za novodobijeni skup podataka.
9. Izračunati matricu korelacije i zapisati je u datoteku.
10. Prikazati matricu korelacije kao sliku. Ćelije u zapisanoj tabeli obojiti spram vrednosti u matrici korelacije.
11. Izdvojiti kolone date, m33, m41, m42, m43, m44 i m45. Za parove date i m kolona napraviti grafikone.
12. Napraviti model za predikciju vrednosti kolone m33 na osnovu meteoroloških podataka. Kao ulaz koristiti podatak o temperaturi. Evaluirati tačnost modela. Iscrtati grafik predikcije i izmerenih vrednosti za kolonu m33.
13. Prepraviti model tako da u obzir uzima i ostale meteorološke podatke, evaluirati tačnost modela i iscrtati grafik za predikcije i izmerene vrednosti kolone m33.
14. Identifikovati kolone koje sadrže nedostajuće vrednosti.
15. Za kolone koje sadrže nedostajuće vrednosti napraviti model koji će dopuniti nedostajuće vrednosti. Rezultat sa dopunjenim vrednostima zapisati u odvojenu datoteku.