# Vežbe 4
*Cilj vežbi: upotreba klasifikatora nad slikama.*

1. Učitati slike iz mnist skupa podataka. Pripremiti slike za obučavanje klasifikatora.
2. Obučiti perceptron za klasifikaciju slika. Odrediti tačnost dobijenog modela.
3. Obučiti logističku regresiju za klasifikaciju slika. Odrediti tačnost dobijenog modela.
4. Obučiti KNN za klasifikaciju slika. Odrediti tačnost dobijenog modela.