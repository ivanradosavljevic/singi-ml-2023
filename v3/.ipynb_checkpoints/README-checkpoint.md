# Vežbe 3
*Cilj vežbi: Rad sa tekstom, učitavanje teksta i njegova obrada.*


1. Napraviti funkciju za učitavanje teksta i njegovu podelu na tokene. Svaki token treba da predstavlja jednu reč iz teksta.
2. Iz teksta izbaciti reči koje ne nose značenje u tekstu.
3. Prebrojati ukupan broj pojava svake reči.
4. Prebrojati broj pozitivnih i broj negativnih dokumenata u skupovima podataka.
5. Prebrojati broj pojava svake reči u negativnom i pozitivnom kontekstu.
6. Napraviti jednostavan klasifikator dokumenata. Izlaz klasifikacije može biti pozitivan ili negativan sentiment.
7. Izračunati vreovatnoće pojave dokumenta i svake reče u negativnom i pozitivnom kontekstu.
8. Napraviti algoritam za određivanje sentimenta dokumenta upotrebom Naive Bayes klasifikatora.